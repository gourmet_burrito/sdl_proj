#pragma once
#include <SDL.h>
#include "Texture.h"

class Ball
{
public:
	Ball(SDL_Renderer *sdl_renderer);
	void setVel(int x, int y);
	void setPos(int x, int y);
	void pos(int *x, int *y);
	void vel(int *x, int *y);
	void render();
	void update();

	~Ball();

private:
	int pos_x, pos_y;
	int radius;
	float vel_x, vel_y;
	bool touched;
	SDL_Renderer *renderer;
	Texture *spritesheet;
};

