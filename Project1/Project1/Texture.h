#pragma once
#include <iostream>
#include <SDL.h>
#include <SDL_image.h>

class Texture
{
public:
	Texture();
	~Texture();
	bool loadFromFile(SDL_Renderer *renderer, std::string path);
	SDL_Texture * getTexture();
	void free();

	int getWidth();
	int getHeight();

private:
	int width, height;
	SDL_Texture *texture;
};

