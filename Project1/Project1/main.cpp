#include <Windows.h>
#include <iostream>
#include <math.h>
#include <ctime>

#include <algorithm>
#include <vector>

#include <GL\glew.h>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>

#include "Texture.h"
#include "Sprite.h"
#include "Ball.h"

const int PI = 3.14159265;

const int WIDTH = 1920, HEIGHT = 600;
SDL_Window *g_window = NULL;
SDL_Renderer *g_renderer = NULL;
SDL_Surface *g_screenSurface = NULL;
SDL_Event g_event;

bool quit = false;

int g_time = 0;

bool init() {
	bool success = true;

	/*
	SDL_INIT_EVERYTHING initializes:
		SDL_INIT_TIMER
		SDL_INIT_AUDIO
		SDL_INIT_VIDEO
		SDL_INIT_JOYSTICK
		SDL_INIT_HAPTIC
		SDL_INIT_GAMECONTROLLER
		SDL_INIT_EVENTS
	*/
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		std::cout << "SDL could not initialize: " << SDL_GetError() << std::endl;
		return false;
	}

	g_window = SDL_CreateWindow("SDL mania",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		WIDTH, HEIGHT,
		SDL_WINDOW_ALLOW_HIGHDPI);

	if (g_window == NULL) {
		std::cout << "Could not create window: " << SDL_GetError() << std::endl;
		return false;
	}

	g_renderer = SDL_CreateRenderer(g_window,
		-1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if (g_renderer == NULL) {
		std::cout << "Could not create renderer: " << SDL_GetError() << std::endl;
		return false;
	}

	int imgFlags = IMG_INIT_PNG;
	if ((!IMG_Init(imgFlags) & imgFlags)) {
		std::cout << "Could not initialize SDL_img PNG: " << SDL_GetError << std::endl;
		return false;
	}

	g_screenSurface = SDL_GetWindowSurface(g_window);

	return success;
}

void close() {
	SDL_FreeSurface(g_screenSurface);
	g_screenSurface = NULL;

	SDL_DestroyRenderer(g_renderer);
	g_renderer = NULL;

	SDL_DestroyWindow(g_window);
	g_window = NULL;

	SDL_Quit();
}


bool t_sort(SDL_Point i, SDL_Point j) { return (i.x<j.x); }

void gen_terrain(std::vector <SDL_Point> *terrain, SDL_Point start, SDL_Point end, float roughness, int displacement, int iterations) {
	terrain->push_back(start);
	terrain->push_back(end);

	int it = 1;
	while (it <= iterations) {
		
		std::vector <SDL_Point> terr(*terrain);
		
		for (int i = 0; i < terr.size()-1; i++) {
			
			SDL_Point pt{
				(terr.at(i).x + terr.at(i + 1).x) / 2,
				(terr.at(i).y + terr.at(i + 1).y) / 2,
			};

			int random_offset = 0;
			if (displacement > 0) { 
				random_offset = (rand() % (2*displacement) + 1) - displacement; 
			}

			pt.y = pt.y + random_offset;

			terrain->push_back(pt);
			std::sort(terrain->begin(), terrain->end(), t_sort);
		}
		
		displacement *= pow(2, -roughness);
		it++;
	}
}

void draw_terrain(std::vector <SDL_Point> *terrain) {
	for (int t = 0; t < terrain->size() - 1; t++) {
		float m = (float)(terrain->at(t+1).y - terrain->at(t).y) / (terrain->at(t+1).x - terrain->at(t).x);

		int b = terrain->at(t).y;
		int x_offset = terrain->at(t).x;

		for (int x = 0; x < (terrain->at(t + 1).x - terrain->at(t).x); x++)
		{
			float y = m*x + b;
			SDL_RenderDrawLine(g_renderer, x+x_offset, HEIGHT, x+x_offset, HEIGHT - y);
		}
	}	
}

void gen_fill_terrain(std::vector <SDL_Point> *src_terr, std::vector <SDL_Point> *filled_terr) {
	for (int t = 0; t < src_terr->size() - 1; t++) {
		float m = (float)(src_terr->at(t + 1).y - src_terr->at(t).y) / (src_terr->at(t + 1).x - src_terr->at(t).x);

		int b = src_terr->at(t).y;
		int x_offset = src_terr->at(t).x;

		for (int x = 0; x < (src_terr->at(t + 1).x - src_terr->at(t).x); x++)
		{
			float y = m*x + b;
			SDL_Point pt = { x, y };
			filled_terr->push_back(pt);
		}
	}
}

void draw_filled_terrain(std::vector <SDL_Point> *terrain) {
	for (int x = 0; x < terrain->size() - 1; x++) {
		int y = terrain->at(x).y;
		SDL_RenderDrawLine(g_renderer, x, HEIGHT, x, HEIGHT - y);
	}
}


void explode(std::vector <SDL_Point> *terrain, int x, int y) {
	int r = 10;
	float px = 0;
	float py = 0;

	for (int i = -r; i < r+1; i += 1) {
		px = x+i;
		py = sqrt((r*r) - (i*i));
		terrain->at(px).y = y - py;
	}
}

int main(int argc, char *argv[]) {
	if (!init()) {
		std::cout << "Failed to initialize" << std::endl;
		close();
		return EXIT_FAILURE;
	}
	srand(time(NULL));

	SDL_Point start = { 0, 100 };
	SDL_Point end = { WIDTH, 300 };

	std::vector <SDL_Point> terrain1;
	std::vector <SDL_Point> terrain2;
	std::vector <SDL_Point> terrain3;
	std::vector <SDL_Point> terrain;

	SDL_Point projectile = { 0, 0 };
	float vel_x = 0;
	float vel_y = 0;
	bool firing = 0;

	gen_terrain(&terrain1, SDL_Point{ 0, 300 }, SDL_Point{ WIDTH,400 }, 1.0, 250, 10); //10
	gen_terrain(&terrain2, SDL_Point{ 0, 250 }, SDL_Point{ WIDTH,300 }, 1.2, 225, 10);//8
	gen_terrain(&terrain3, SDL_Point{ 0, 200 }, SDL_Point{ WIDTH,200 }, 1.0, 200, 10);//5

	gen_fill_terrain(&terrain3, &terrain);

	int player_x = rand() % WIDTH + 1;
	int player_y = terrain.at(player_x).y;
	float trajectory = 0;
	int aim_x = 0;
	int aim_y = 0;

	int m_x, m_y = 0;
	Uint32 mouseState;

	const Uint8 *keyStates = SDL_GetKeyboardState(NULL);
	while (!quit) {
		while (SDL_PollEvent(&g_event) != 0) {
			if (g_event.type == SDL_QUIT) {
				quit = true;
			}
		}
		
		if (keyStates[SDL_SCANCODE_A]) {
			trajectory += 2.0;
		}
		if (keyStates[SDL_SCANCODE_D]) {
			trajectory -= 2.0;
		}
		if (keyStates[SDL_SCANCODE_F]) {
			projectile.x = player_x-aim_x;
			projectile.y = player_y+aim_y;
			vel_x = aim_x / 1.5;
			vel_y = aim_y / 1.5;
			firing = 1;
		}

		if (firing) {		
			projectile.x -= vel_x;
			projectile.y += vel_y;
			vel_y = vel_y - 0.25;
			vel_x += 0.0;
		}
		if (projectile.x < 0 || projectile.x > WIDTH) {
			firing = 0;
			projectile.x = 0;
			projectile.y = 0;
		}

		if (firing && (projectile.y < terrain.at(projectile.x).y)) {
			explode(&terrain, projectile.x, projectile.y);
			firing = 0;
			projectile.x = 0;
			projectile.y = 0;
		}

		if (projectile.y < 0) {
			firing = 0;
			projectile.x = 0;
			projectile.y = 0;
		}		

		//std::cout << projectile.x<< " " << projectile.y << "\r";

		aim_x = 20*sin(trajectory*PI / 180);
		aim_y = 20*cos(trajectory*PI / 180);
		
		//SDL_SetRenderDrawColor(g_renderer, 0x00, 0x00, 0x00, 0x00);
		SDL_SetRenderDrawColor(g_renderer, 0x93, 0xC0, 0xDE, 0x00);
		SDL_RenderClear(g_renderer);
		SDL_SetRenderDrawColor(g_renderer, 0x60, 0xFF, 0x60, 0x00);
		draw_terrain(&terrain1);
		SDL_SetRenderDrawColor(g_renderer, 0x30, 0x99, 0x60, 0x00);
		draw_terrain(&terrain2);
		SDL_SetRenderDrawColor(g_renderer, 0x00, 0x55, 0x00, 0x00);
		draw_filled_terrain(&terrain);
		
		SDL_SetRenderDrawColor(g_renderer, 0xFF, 0xFF, 0xFF, 0x00);
		SDL_RenderDrawLine(g_renderer, player_x, HEIGHT-player_y, player_x-aim_x, (HEIGHT-player_y)-aim_y);

		SDL_SetRenderDrawColor(g_renderer, 0xFF, 0x00, 0x00, 0x00);
		SDL_RenderSetScale(g_renderer, 2.5, 2.5);		
		SDL_RenderDrawPoint(g_renderer, (projectile.x)/2.5, (HEIGHT-projectile.y)/2.5);
		SDL_RenderSetScale(g_renderer, 1, 1);

		SDL_RenderPresent(g_renderer);

		if ((SDL_GetTicks() - g_time) < (1000 / 60)) {
			SDL_Delay((1000 / 24) - (SDL_GetTicks() - g_time));
		}
		g_time = SDL_GetTicks();

	}
	close();
	return EXIT_SUCCESS;
}