#include "Sprite.h"

Sprite::Sprite()
{
	pos_x = pos_y = 0;
	vel_y = vel_y = 0;
	dir_x = dir_y = 1;
	state = IDLE;
	texture_index = 0;
	spritesheet = NULL;
	flip = SDL_FLIP_NONE;
}


Sprite::~Sprite()
{
	if (spritesheet != NULL) {
		spritesheet->free();
	}
	spritesheet = NULL;
}

void Sprite::setSpritesheet(SDL_Renderer *renderer, std::string path) {
	spritesheet = new Texture();
	spritesheet->loadFromFile(renderer, path);
}

void Sprite::setDirection(int x, int y) {
	dir_x = x;
	dir_y = y;
}

void Sprite::setPosition(int x, int y) {
	pos_x = x;
	pos_y = y;
}

void Sprite::pos(int *x, int *y)
{
	*x = pos_x;
	*y = pos_y;
}

void Sprite::vel(int *x, int *y)
{
	*x = vel_x;
	*y = vel_y;
}

void Sprite::setState(State s) {
	state = s;
}

void Sprite::update() {
	pos_x += vel_x;
	pos_y -= vel_y;
}

void Sprite::_computeFlip() {
	flip = SDL_FLIP_NONE;
	if (dir_x < 0) {
		flip = SDL_FLIP_HORIZONTAL;
	}
	if (dir_y < 0) {
		flip = (SDL_RendererFlip)(flip | SDL_FLIP_VERTICAL);
	}
}

void Sprite::render(SDL_Renderer *renderer) {
	_computeFlip();

	int x_idx = 0;
	int y_idx = 0;

	SDL_Rect src = { TILE_SIZE*x_idx, TILE_SIZE*y_idx,256,256 };
	SDL_Rect dst = { pos_x, pos_y, 256,256 };

	SDL_RenderCopyEx(renderer,
		spritesheet->getTexture(),
		&src,
		&dst,
		0,
		NULL,
		flip);
	/*
		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		for (int i = 0; i < 360; i++) {
			int x = (100 * sin(i)) + pos_x + 128;
			int y = (100 * cos(i)) + pos_y+115;
			SDL_RenderDrawPoint(renderer, x, y);
		}
	*/

}