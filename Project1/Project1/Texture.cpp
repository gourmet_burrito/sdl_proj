#include "Texture.h"


Texture::Texture()
{
	width = 0;
	height = 0;
	texture = NULL;
}


Texture::~Texture()
{
	free();
}

int Texture::getWidth()
{
	return width;
}

int Texture::getHeight()
{
	return height;
}

SDL_Texture * Texture::getTexture() {
	return texture;
}

void Texture::free()
{
	if (texture != NULL) {
		SDL_DestroyTexture(texture);
		texture = NULL;
		width = 0;
		height = 0;
	}
}

bool Texture::loadFromFile(SDL_Renderer *renderer, std::string path)
{
	free();

	SDL_Surface *surface = IMG_Load(path.c_str());
	if (!surface) {
		std::cout << "Unable to load image: " << SDL_GetError() << std::endl;
		return false;
	}

	SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGBA(surface->format, 0x00, 0x00, 0x00, 0x00));
	texture = SDL_CreateTextureFromSurface(renderer, surface);
	if (!texture) {
		std::cout << "Unable to create texture: " << SDL_GetError() << std::endl;
		return false;
	}

	width = surface->w;
	height = surface->h;

	SDL_FreeSurface(surface);
	return true;
}
