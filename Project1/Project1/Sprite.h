#pragma once
#include <iostream>
#include "Texture.h"

const int TILE_SIZE = 256;

enum State {
	IDLE = 1,
	RUN = 2,
	JUMP = 4,
	SHOOT = 8,
	HIT = 16,
	CLIMB = 32,
	DEATH = 64
};

struct SS {
	int idle;
	int run;
	int jump;
	int shoot;
	int hit;
	int climb;
	int death;
};

inline State operator|(State a, State b)
{
	return static_cast<State>(static_cast<int>(a) | static_cast<int>(b));
}

inline State operator&(State a, State b)
{
	return static_cast<State>(static_cast<int>(a) & static_cast<int>(b));
}

class Sprite
{
public:
	Sprite();
	~Sprite();
	void render(SDL_Renderer *sdl_renderer);
	void setSpritesheet(SDL_Renderer *sdl_renderer, std::string path);
	void update();
	
	void setState(State s);
	void setDirection(int x, int y);
	void setPosition(int x, int y);	
	void pos(int *x, int *y);
	void vel(int *x, int *y);

private:
	void Sprite::_computeFlip();
	int texture_index;
	int pos_x, pos_y;
	int vel_x, vel_y;
	int dir_x, dir_y;
	SDL_Renderer *renderer;
	Texture *spritesheet;
	State state;
	SDL_RendererFlip flip;
};