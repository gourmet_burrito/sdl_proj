#include "Ball.h"



Ball::Ball(SDL_Renderer *sdl_renderer)
{
	renderer = sdl_renderer;
	pos_x = 200;
	pos_y = 200;
	radius = 20;
	vel_x = 0;
	vel_y = 0;
	spritesheet = new Texture();
	spritesheet->loadFromFile(renderer, "imgs/ball.png");
	bool touched = false;
}


Ball::~Ball()
{
}

void Ball::setVel(int x, int y)
{
	vel_x = x;
	vel_y = y;	
	touched = true;
}

void Ball::update()
{
	if (pos_x + vel_x >= 800 || pos_x + vel_x <=0) {
		vel_x = -vel_x;
	}

	if (pos_y + vel_y >= 480 || pos_y + vel_y <= 20) {
		vel_y = -vel_y;
	}

	pos_x += vel_x;
	pos_y += vel_y;

	if (pos_y <= 480 && touched) {
		vel_y += 3;
	}

	vel_x *= 0.98;
	vel_y *= 0.98;
}

void Ball::render()
{
	int x_idx = 0;
	int y_idx = 0;

	SDL_Rect src = { 256 * x_idx, 256 * y_idx,256,256 };
	SDL_Rect dst = { pos_x, pos_y, 128,128 };

	SDL_RenderCopyEx(renderer,
		spritesheet->getTexture(),
		&src,
		&dst,
		0,
		NULL,
		SDL_FLIP_NONE);

}

void Ball::pos(int *x, int *y)
{
	*x = pos_x;
	*y = pos_y;
}

void Ball::vel(int *x, int *y)
{
	*x = vel_x;
	*y = vel_y;
}